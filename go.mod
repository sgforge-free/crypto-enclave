module crypto-enclave

go 1.18

require github.com/google/go-tpm v0.3.3

require golang.org/x/sys v0.0.0-20210629170331-7dc0b73dc9fb // indirect
