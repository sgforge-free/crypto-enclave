package main

import (
	"github.com/google/go-tpm/tpm2"
	"github.com/google/go-tpm/tpmutil"
)

func main() {

	const (
		EKReservedHandle  = tpmutil.Handle(0x81010001)
		SRKReservedHandle = tpmutil.Handle(0x81000001)
	)
	const CertifyHashAlgTpm = tpm2.AlgSHA256
	const NumPCRs = 24

}
